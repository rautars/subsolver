function love.conf(t)
    t.window.title = "The Subsolver"
    t.window.width = 640
    t.window.height = 800
    t.window.fullscreen = false
    t.window.vsync = true
    t.modules.joystick = true
    t.modules.physics = true
    t.modules.audio = true
    t.modules.keyboard = true
    t.modules.event = true
    t.modules.image = true
    t.modules.graphics = true
end