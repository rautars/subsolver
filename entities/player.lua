Player = {}
Player.__index = Player

function Player:new(x, y)
    local player = {
        x = x,
        y = y,
        speed = 0,
        maxSpeed = 300,
        image = love.graphics.newImage("assets/images/player.png"),
        rocketFiredSound = love.audio.newSource("assets/sounds/Bottle Rocket-SoundBible.com-332895117.mp3", "stream"),
        direction = 1,
        rockets = {},
        shootCooldown = 0
    }
    player.rocketFiredSound:setVolume(0.2)
    setmetatable(player, Player)
    return player
end

function Player:update(dt)
    self.speed = 150 - math.min(dt * 10, 1)

    if love.keyboard.isDown("up") then
        self.y = self.y - self.speed * dt
    end
    if love.keyboard.isDown("down") then
        self.y = self.y + self.speed * dt
    end
    if love.keyboard.isDown("left") then
        self.x = self.x - self.speed * dt
        self.direction = -1
    end
    if love.keyboard.isDown("right") then
        self.x = self.x + self.speed * dt
        self.direction = 1
    end
    if love.keyboard.isDown("space") then
        self:shoot()
    end

    -- cap the player's max speed
    self.speed = math.min(self.speed, self.maxSpeed)
    self.speed = math.max(self.speed, -self.maxSpeed)

    -- check if the player is out of bounds
    if self.x < 0 + 96 then
        self.x = 0 + 96
    elseif self.x > love.graphics.getWidth() - 96 then -- width of the player + the width of the pipe
        self.x = love.graphics.getWidth() - 96
    end

    if self.y < 60 then
        self.y = 60
    elseif self.y > love.graphics.getHeight() - 32 then
        self.y = love.graphics.getHeight() - 32
    end

    for i = #self.rockets, 1, -1 do
        local rocket = self.rockets[i]
        rocket.x = rocket.x + rocket.speed * dt
        -- remove rocket if it goes off the screen
        if rocket.x < 0 or rocket.x > love.graphics.getWidth() then
            table.remove(self.rockets, i)
        end
    end

    -- event for collision detection
    if #self.rockets > 0 then
        love.event.push("rocketsFired", self.rockets)
    end

    if self.shootCooldown > 0 then
        self.shootCooldown = self.shootCooldown - dt
    end
end

function Player:shoot()
    if self.shootCooldown <= 0 then
        local rocket = {
            x = self.x + 8 * self.direction,
            y = self.y,
            width = 6,
            height = 3,
            dead = false,
            speed = 200 * self.direction
        }
        table.insert(self.rockets, rocket)
        self.shootCooldown = 0.6

        -- play rocket fired sound
        local newRocketFiredSound = self.rocketFiredSound:clone()
        love.audio.play(newRocketFiredSound)
    end
end

function Player:draw()
    -- draw rockets
    for _, rocket in ipairs(self.rockets) do
        love.graphics.rectangle("fill", rocket.x, rocket.y, rocket.width, rocket.height)
    end

    -- Get image width and height
    local imageWidth = self.image:getWidth()
    local imageHeight = self.image:getHeight()

    -- Calculate flip offset
    local flipOffset = 16

    -- draw player
    love.graphics.draw(self.image, self.x, self.y, 0, self.direction, 1, flipOffset, imageHeight / 2)
end

return Player
