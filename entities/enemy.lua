local Animation = require("utilities.animation")

local Enemy = {}

function Enemy:new(x, y)
    local enemy = {
        x = x,
        y = y,
        width = 32,
        height = 32,
        dead = false
    }
    local animationDuration = 1.2 -- 1.2 second
    local spriteSheet = love.graphics.newImage("assets/images/enemy.png")
    enemy.animation = Animation:new(spriteSheet, enemy.width, enemy.height, animationDuration)

    setmetatable(enemy, { __index = Enemy })
    return enemy
end

function Enemy:update(dt)
    self.animation:update(dt)

    if not self.dead then
        local RocketFired = love.thread.getChannel("RocketFired"):pop()
        if RocketFired then
            if RocketFired.x > self.x and RocketFired.x < self.x + self.image:getWidth() and RocketFired.y > self.y and RocketFired.y < self.y + self.image:getHeight() then
                self.dead = true
                love.event.push("enemyHit", self)
            end
        end
    end
end

function Enemy:draw()
    self.animation:draw(self.x, self.y)
end

return Enemy