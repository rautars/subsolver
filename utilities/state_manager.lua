local stateManager = {}

local currentState = nil

function stateManager.switchState(newState)
  if currentState and currentState.exit then
    currentState.exit(currentState)
  end

  currentState = newState

  if currentState and currentState.enter then
    currentState.enter(currentState)
  end
end

function stateManager.getCurrentState()
  return currentState
end

return stateManager
