local Animation = {}
Animation.__index = Animation

function Animation:new(image, width, height, duration)
    local self = setmetatable({}, Animation)
    self.image = image
    self.width = width
    self.height = height
    self.duration = duration or 1
    self.currentTime = 0

    self.frames = {}
    local xCount = math.floor(image:getWidth() / width)
    local yCount = math.floor(image:getHeight() / height)

    for y = 0, yCount - 1 do
        for x = 0, xCount - 1 do
            local quad = love.graphics.newQuad(x * width, y * height, width, height, image:getDimensions())
            table.insert(self.frames, quad)
        end
    end

    return self
end

function Animation:update(dt)
    self.currentTime = self.currentTime + dt
    if self.currentTime >= self.duration then
        self.currentTime = self.currentTime - self.duration
    end
end

function Animation:draw(x, y)
    local frame = math.floor(self.currentTime / self.duration * #self.frames) + 1
    love.graphics.draw(self.image, self.frames[frame], x, y)
end

return Animation
