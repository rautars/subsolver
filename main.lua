local stateManager = require("utilities.state_manager")
local menu = require("scenes.menu")
local level1 = require("scenes.level1")
local gameover = require("scenes.gameover")

function love.load()
    stateManager.switchState(menu)
end

function love.update(dt)
    local currentState = stateManager.getCurrentState()
    if currentState and currentState.update then
        currentState.update(currentState, dt)
    end
end

function love.draw()
    local currentState = stateManager.getCurrentState()
    if currentState and currentState.draw then
        currentState.draw(currentState)
    end
end

function menu:update(dt)
    if love.keyboard.isDown("return") then
        stateManager.switchState(level1)
    end
end

function gameover:update(dt)
    if love.keyboard.isDown("return") then
        stateManager.switchState(menu)
    end
end
