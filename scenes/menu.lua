local Menu = {}

function Menu:enter()
    timer = 60
    backgroundImage = love.graphics.newImage("assets/images/menu.png")
end

function Menu:exit()
end

function Menu:update(dt)

end

function Menu:draw()
    love.graphics.draw(backgroundImage, 0, 0)
    love.graphics.print("Press Enter to Start", 240, 700)
end

return Menu