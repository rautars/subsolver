GameOver = {}

function GameOver:enter()
end

function GameOver:exit()
end

function GameOver:update(dt)

end

function GameOver:draw()
    love.graphics.printf("Thank You for playing!", 0, 150, love.graphics.getWidth(), "center")
    love.graphics.printf("The average connection was: " .. string.format("%.3f", connectionAvg), 0, 170, love.graphics.getWidth(), "center")
    love.graphics.printf("The total number of defeated monsters: " .. totalEnemyDefeated, 0, 190, love.graphics.getWidth(), "center")


    love.graphics.printf("Press Enter to restart", 0, 700, love.graphics.getWidth(), "center")

    love.graphics.printf("Made by gitlab.com/rautars for Love2D game jam", 0, 650, love.graphics.getWidth(), "center")
end

return GameOver