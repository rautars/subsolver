local stateManager = require("utilities.state_manager")
local Player = require("entities.player")
local Enemy = require("entities.enemy")
local gameover = require("scenes.gameover")
local sti = require("lib.sti")
local gameMap = sti("assets/tiled-project/level1.lua")
local Level1 = {}

function drawBoid (mode, x, y, length, width, angle)
    -- position, length, width and angle
    love.graphics.push()
    love.graphics.translate(x, y)
    love.graphics.rotate(angle)
    love.graphics.polygon(mode, -length / 2, -width / 2, -length / 2, width / 2, length / 2, 0)
    love.graphics.pop()
end

function Level1:enter()
    player = Player:new(300, 400)
    spawnPoints = {}
    enemies = {}
    leftConnection = 3
    rightConnection = 3
    totalEnemyDefeated = 0
    timer = 60
    connectionStatistics = {}
    connectionAvg = 3
    font = love.graphics.newFont(16)
    love.graphics.setFont(font)
    lastTime = love.timer.getTime()
    recordInterval = 1
    conLabelImage = love.graphics.newImage("assets/images/con_label.png")
    leftCon1Image = love.graphics.newImage("assets/images/left_con1.png")
    leftCon2Image = love.graphics.newImage("assets/images/left_con2.png")
    leftCon3Image = love.graphics.newImage("assets/images/left_con3.png")
    leftCon4Image = love.graphics.newImage("assets/images/left_con4.png")
    rightCon1Image = love.graphics.newImage("assets/images/right_con1.png")
    rightCon2Image = love.graphics.newImage("assets/images/right_con2.png")
    rightCon3Image = love.graphics.newImage("assets/images/right_con3.png")
    rightCon4Image = love.graphics.newImage("assets/images/right_con4.png")

    -- Set the random seed to the current system time
    math.randomseed(os.time())

    for i = 0, 18 do
        local y = 70 + i * 32
        table.insert(spawnPoints, { 32, y })
        table.insert(spawnPoints, { 576, y })
    end
end

function Level1:exit()
end

function calculateConAverage()
    if #connectionStatistics > 0 then
        local sum = 0
        local count = 0
        for i, v in ipairs(connectionStatistics) do
            sum = sum + v
            count = count + 1
        end
        connectionAvg = sum / count
    end
end

function Level1:update(dt)
    player:update(dt)
    timer = timer - dt
    if timer <= 0 then
        stateManager.switchState(gameover)
    end

    if #enemies < 3 and math.random() < dt then
        local spawnPoint = spawnPoints[math.random(#spawnPoints)]

        local spawnOccupied = false
        for i, enemy in ipairs(enemies) do
            if enemy.x == spawnPoint[1] and enemy.y == spawnPoint[2] then
                spawnOccupied = true
                break
            end
        end

        if not spawnOccupied then
            local newEnemy = Enemy:new(spawnPoint[1], spawnPoint[2])
            table.insert(enemies, newEnemy)
            if newEnemy.x > 50 then
                rightConnection = rightConnection - 1
            else
                leftConnection = leftConnection - 1
            end
        end
    end

    -- Update all enemies
    for i = #enemies, 1, -1 do
        local enemy = enemies[i]
        enemy:update(dt)
        if enemy.dead then
            table.remove(enemies, i)
        end
    end

    currentTime = love.timer.getTime()
    if currentTime - lastTime >= recordInterval then
        table.insert(connectionStatistics, leftConnection + rightConnection)
        lastTime = currentTime
    end
    calculateConAverage()
end

function Level1:draw()
    drawConnectionStatus()
    love.graphics.stencil(function()
        angle = math.pi
        x = player.x + 90
        if player.direction == -1 then
            angle = math.pi * 2
            x = player.x - 90
        end
        y = player.y
        drawBoid("fill", x, y, 160, 60, angle)
    end, "replace", 1)
    love.graphics.setStencilTest("greater", 0)

    angle = math.pi
    x = player.x + 90
    if player.direction == -1 then
        angle = math.pi * 2
        x = player.x - 90
    end
    y = player.y
    drawBoid("fill", x, y, 160, 60, angle)
    gameMap:draw()
    for i, enemy in ipairs(enemies) do
        enemy:draw()
    end
    -- reset stencil test to default
    love.graphics.setStencilTest()

    player:draw()
    love.graphics.printf("Time remaining: " .. math.floor(timer), 0, 40, love.graphics.getWidth(), "center")
end

function drawConnectionStatus()
    love.graphics.draw(conLabelImage, 0, 0)
    if leftConnection == 3 then
        love.graphics.draw(leftCon4Image, 0, 0)
    end
    if leftConnection == 2 then
        love.graphics.draw(leftCon3Image, 0, 0)
    end
    if leftConnection == 1 then
        love.graphics.draw(leftCon2Image, 0, 0)
    end
    if leftConnection == 0 then
        love.graphics.draw(leftCon1Image, 0, 0)
    end

    if rightConnection == 3 then
        love.graphics.draw(rightCon4Image, 0, 0)
    end
    if rightConnection == 2 then
        love.graphics.draw(rightCon3Image, 0, 0)
    end
    if rightConnection == 1 then
        love.graphics.draw(rightCon2Image, 0, 0)
    end
    if rightConnection == 0 then
        love.graphics.draw(rightCon1Image, 0, 0)
    end
end

function checkCollision(rocketX, rocketY, rocketWidth, rocketHeight, imageX, imageY, imageWidth, imageHeight)
    if rocketX + rocketWidth > imageX and
            rocketX < imageX + imageWidth and
            rocketY + rocketHeight > imageY and
            rocketY < imageY + imageHeight then
        return true
    else
        return false
    end
end

love.handlers["rocketsFired"] = function()
    for i, enemy in ipairs(enemies) do
        for j, rocket in ipairs(player.rockets) do
            if checkCollision(rocket.x, rocket.y, 6, 3, enemy.x, enemy.y, enemy.width, enemy.height) then
                enemy.dead = true
                rocket.dead = true
                love.event.push("enemyHit")
            end
        end
    end
end

love.handlers["enemyHit"] = function()
    for i, enemy in ipairs(enemies) do
        if enemy.dead then
            table.remove(enemies, i)
            if enemy.x > 50 then
                rightConnection = rightConnection + 1
            else
                leftConnection = leftConnection + 1
            end
            totalEnemyDefeated = totalEnemyDefeated + 1
            break
        end
    end
    for i = #player.rockets, 1, -1 do
        local rocket = player.rockets[i]
        if rocket.dead then
            table.remove(player.rockets, i)
        end
    end
end

return Level1