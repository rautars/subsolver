# The Subsolver
A mini game made for Love2D 2023 game jam.

## Requirements
This game requires [LÖVE](https://love2d.org/) framework installed.

## How to Play
Shoot down sea monsters from connection lines to improve average connection score.

Use keyboard `arrows` to navigate submarine and `space` to shoot use space.

Pay attention to top connection bars as lower they are the more monsters on that side. 

## Screenshots
![screenshot](https://gitlab.com/rautars/subsolver/-/raw/main/assets/screenshots/subsolver_v1.jpg)

## Credits

### Code
- rautars

### Art
- routars

### Audio
- Mike Koenig, [SoundBible.com](https://soundbible.com/709-Bottle-Rocket.html) - fired rocket sound